# BALL TRACKING ALGORITHM IN ROS

<img src="images/ball_tracking.png"  width="500" height="500">


This is the ball tracking algorithm developed in [1] and adapted for ROS. 

It uses Basler cameras:
https://www.baslerweb.com/.

The setup for tracking the ball is shown in the following figure:

<img src="images/camera_Setup.png"  width="300" height="500">


It works on Ubuntu 18.04LTS, running Ros melodic

_____________________
[1] Vishniakou, I., Plöger, P. G., & Seelig, J. D. (2019). Virtual reality for animal navigation with camera-based optical flow tracking. Journal of Neuroscience Methods, 327, 108403.


# Installation

## INSTALL ROS
Install ROS: 
http://wiki.ros.org/melodic/Installation/Ubuntu

Then create a catkin workspace: 
http://wiki.ros.org/catkin/Tutorials/create_a_workspace


## CLONE BALL TRACKING REPO
> git clone gitlab.com/anflores/ball_tracking


## INSTALL PYLON5

Copy the folder python5 from the repository in the directory:
> ~/YOUR_CATKIN_WORKSPACE/libraries/pylon5


The opencv version needed to run the ball tracking algorithm is 3.2.0


## INSTALL OPENCV

### 1. KEEP UBUNTU UP TO DATE

> sudo apt-get -y update

> sudo apt-get -y upgrade

> sudo apt-get -y autoremove


### 2. INSTALL THE DEPENDENCIES

Build tools:
> sudo apt-get install -y build-essential cmake

GUI (if you want to use GTK instead of Qt, replace 'qt5-default' with 'libgtkglext1-dev' and remove '-DWITH_QT=ON' option in CMake):
> sudo apt-get install -y libgtkglext1-dev libvtk6-dev

Media I/O:
> sudo apt-get install -y zlib1g-dev libjpeg-dev libwebp-dev libpng-dev libtiff5-dev libjasper-dev libopenexr-dev libgdal-dev libgphoto2-dev

Video I/O:
> sudo apt-get install -y libdc1394-22-dev libavcodec-dev libavformat-dev libswscale-dev libtheora-dev libvorbis-dev libxvidcore-dev libx264-dev yasm libopencore-amrnb-dev libopencore-amrwb-dev libv4l-dev libxine2-dev v4l-utils

Parallelism and linear algebra libraries:
>sudo apt-get install -y libtbb-dev libeigen3-dev

Python:
> sudo apt-get install -y python-dev python-tk python-numpy python3-dev python3-tk python3-numpy



### 3. INSTALL THE LIBRARY (3.2.0)

> sudo apt-get install -y unzip wget

> wget https://github.com/opencv/opencv/archive/3.2.0.zip -O opencv320.zip

> unzip opencv320.zip

> rm opencv320.zip

> mv opencv-3.2.0 OpenCV

> cd OpenCV

> touch OpenCV3.2withContrib


### 4. INSTALL THE OPENCV_CONTRIB LIBRARY (3.2.0)
> wget https://github.com/opencv/opencv_contrib/archive/3.2.0.zip -O opencv_contrib320.zip

> unzip opencv_contrib320.zip

> rm opencv_contrib320.zip

> mv opencv_contrib-3.2.0 OpenCV_contrib


### 5. Build OpenCV with contrib
> mkdir build

> cd build

> cmake -DOPENCV_EXTRA_MODULES_PATH=../OpenCV_contrib/modules -DWITH_QT=OFF -DWITH_OPENGL=ON 
-DFORCE_VTK=ON -DWITH_TBB=ON -DINSTALL_C_EXAMPLES=OFF -DWITH_GDAL=ON -DWITH_XINE=ON -DBUILD_EXAMPLES=OFF -DENABLE_PRECOMPILED_HEADERS=OFF ..

> make -j`nproc`

> sudo make install

> sudo ldconfig

## Build Ball tracking

> cd YOUR_CATKIN_WORKSPACE/

> catkin_make --only-pkg-with-deps ball_tracking


# INSTALL PYLONVIEWER
Download pylonviewer from here and follow installation instructions: 
https://www.baslerweb.com/en/products/basler-pylon-camera-software-suite/pylon-viewer/

Once it is installed, connect your pylon camera to the computer and open pylonviewer. Open the camera and configure the settings.

The camera must only ouput 1 channel (monochromatic), and the resolution of the field of view typically used is 224 x 140 (width x height)

 Save the settings in:
> YOUR_CATKIN_WORKSPACE/ball_tracking/config/camera_settings.pfs






# RUN Ball tracking

If everything went good, you should be able to run 

> rosrun ball_tracking ball_tracking

This algorithm publishes data in 4 topics:

> rostopic list



> /ball/tracking/cmd_vel     --> Velocity of the ball (geometry_msgs/TwistStamped)

> /ball_tracking/displacement --> Displacement of the ball (geometry_msgs/Twist)

> /ball_tracking/time_stamp   --> ROS time stamp (std_msgs/UInt64)

> ball_tracking/tracking_data --> Tracking data of the algorithm (geometry_msgs/PointStamped)

