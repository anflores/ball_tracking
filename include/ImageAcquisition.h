#pragma once
#include <pylon/PylonIncludes.h>
#include <opencv/cv.h>
#include <opencv2/core/core.hpp>
#include <opencv2/core/optim.hpp>
#include <iostream>
#include <string>
#include <omp.h>
#include <boost/lockfree/queue.hpp>
#include <opencv/cv.h>
#include "opencv2/opencv.hpp"
#include "opencv2/imgproc/imgproc.hpp"

class ImageAcquisition
{
private:
	Pylon::CInstantCamera* pCamera;
	Pylon::CImageFormatConverter* pFormatConverter;
	//Pylon::CAviWriter* pAviWriter;

	Pylon::CGrabResultPtr ptrGrabResult;
	Pylon::CPylonImage pylonImage;

	bool aviInitialzed;
	bool cameraInitialized;

public:
	ImageAcquisition();
	static std::vector<std::string> listCameras();
	bool softwareTrigger();
	bool waitTriggerReady();
	bool init(std::string cameraName);
	bool loadCameraSettings(std::string settingsFilename);
	bool setupVideoRecording(char outputFileneame[]);
	bool grabOne(cv::OutputArray frame, uint64& timestamp);
	~ImageAcquisition();
};

