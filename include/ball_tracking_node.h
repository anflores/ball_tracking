#ifndef _ball_tracking_node_
#define _ball_tracking_node_


#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/String.h"
#include "tf/transform_broadcaster.h"
#include "tf/transform_listener.h"
#include "geometry_msgs/PointStamped.h"
#include "visualization_msgs/Marker.h"
#include "geometry_msgs/TwistStamped.h"
#include "std_msgs/UInt64.h"

#include <sstream>
#include <stdio.h>
#include <iostream>
#include <string>
#include <chrono>
#include <vector>
#include <numeric>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

#include <pylon/PylonIncludes.h>
#include <opencv/cv.h>

#include "ImageAcquisition.h"
#include "BallTracking.h"

// [publisher subscriber headers]

// [service client headers]

// [action server client headers]

/**
 * \brief Specific Algorithm Class
 *
 */





class BallTrackingNode
{
  private:

	ros::NodeHandle* nh;

	BallTracking ball_tracker;
	ImageAcquisition imageAcquisition;

	std::string package_dir;
	std::string camera_name;
	std::string camera_settings_file;
	boost::property_tree::ptree tracker_settings;

	bool load_camera_settings;

	cv::Mat timings_plot;
	cv::Mat frame;
	cv::Mat track_data;
	int c;
	std::vector<int> runtimes;
	float fit_quality;
	std::chrono::microseconds prev_iter_time;
	uint64 frame_timestamp;


    cv::Mat ballXYZtoArenaXYZMat;
    cv::Mat ballXYZtoArenaYaw;
    cv::Mat ballRot;
    float gainWalk;
    float gainTurn;

    float x_disp;
    float y_disp;
    float th_disp;

    ros::Time timer_disp;

	// [publisher attributes]
	ros::Publisher TrackingDataPub;
    geometry_msgs::PointStamped trackingData;

    ros::Publisher DisplacementPub;
    geometry_msgs::Twist velCmd;
    geometry_msgs::Twist dispCmd;

    ros::Publisher velCmdPub;
    geometry_msgs::TwistStamped velCmdMsg;

    ros::Publisher ballStampPub;
    std_msgs::UInt64 ball_stamp_msg;


    // [subscriber attributes]
	//ros::Subscriber rosSub;
    

    // [service attributes]

    // [client attributes]

    // [action server attributes]

    // [action client attributes]

    // [transforms]
	//tf::TransformBroadcaster broadcaster;
		

    /********** private methods ************/

	void debug(std::string type, std::string msg);
	bool loadSettings();


    /************ callbacks ************/
	//void subCallback(const std_msgs::String::ConstPtr &msg);

  public:
   /**
    * \brief Constructor
    *
    * This constructor initializes specific class attributes and all ROS
    * communications variables to enable message exchange.
    */
	BallTrackingNode(ros::NodeHandle* n);

   /**
    * \brief Destructor
    *
    * This destructor frees all necessary dynamic memory allocated within this
    * this class.
    */
    ~BallTrackingNode();

  public:
   /**
    * \brief main node thread
    *
    * This is the main thread node function. Code written here will be executed
    * in every node loop while the algorithm is on running state. Loop frequency
    * can be tuned by modifying loop_rate attribute.
    *
    * Here data related to the process loop or to ROS topics (mainly data structs
    * related to the MSG and SRV files) must be updated. ROS publisher objects
    * must publish their data in this process. ROS client servers may also
    * request data to the corresponding server topics.
    */
    void mainNodeThread();

};

#endif

