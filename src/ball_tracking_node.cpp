#include "../include/ball_tracking_node.h"

BallTrackingNode::BallTrackingNode(ros::NodeHandle* n) :
	ball_tracker(true, RotModel::MODE_TRACKING),
	imageAcquisition(){

	nh = n;
	this->debug("INFO", "ball_tracking_node is running!");

	package_dir = ros::package::getPath("ball_tracking");

	load_camera_settings = false;
	if (this->loadSettings()){
		this->debug("INFO", "Loaded camera settings!");
	}
	else{
		this->debug("ERROR", "Failed to load camera settings!");
		return;
	}

	if (imageAcquisition.init(camera_name)) {
		imageAcquisition.loadCameraSettings(camera_settings_file);
	}
	else {
		this->debug("ERROR!", std::string("Failed to open camera ") + camera_name);
		return;
	}

	timings_plot = cv::Mat::zeros(cv::Size(1000, 100), CV_8UC1);
	c = 0;
	prev_iter_time = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch());

	cv::namedWindow("Tracking camera",  cv::WINDOW_AUTOSIZE);


	// [publishers]
	TrackingDataPub = nh->advertise<geometry_msgs::PointStamped>( "/ball_tracking/tracking_data", 0 );
	DisplacementPub = nh->advertise<geometry_msgs::Twist>( "/ball_tracking/displacement", 0); //"ball_tracking/vel_cmd", 0 );
	velCmdPub       = nh->advertise<geometry_msgs::TwistStamped>("/ball_tracking/cmd_vel", 0);
	ballStampPub    = nh->advertise<std_msgs::UInt64>("/ball_tracking/time_stamp", 0);
	// [subscribers]
	//rosSub = nh->subscribe("/BallTracking_sub", 1, &BallTracking::subCallback, this);
    this->timer_disp = ros::Time::now();
    dispCmd.linear.x = 0;
    dispCmd.linear.y = 0;
    dispCmd.linear.z = 0;
	dispCmd.angular.z = 0;
}

BallTrackingNode::~BallTrackingNode(){
	delete nh;
	nh = NULL;
	cv::destroyAllWindows();
}


bool BallTrackingNode::loadSettings(){

	try {
		this->debug("INFO", std::string("Loading camera settings from: ") + package_dir + std::string("/Config/"));
		boost::property_tree::ini_parser::read_ini(package_dir + std::string("/Config/tracking.cfg"), tracker_settings);

		camera_name = tracker_settings.get<std::string>("camera settings.TrackingCameraName", "tracking_cam");
		camera_settings_file = package_dir + std::string("/") + tracker_settings.get<std::string>("camera settings.CameraSettingsFile", "/Config/camera_settings.pfs");
		ball_tracker.parameters.polarCenter.x = tracker_settings.get<float>("tracking settings.BallCenterX");
		ball_tracker.parameters.polarCenter.y = tracker_settings.get<float>("tracking settings.BallCenterY");
		ball_tracker.parameters.visibleBallRadius = tracker_settings.get<float>("tracking settings.BallRadius");

		ball_tracker.parameters.calibrCXYrad = tracker_settings.get<float>("tracking settings.CxyRad");
		ball_tracker.parameters.calibrCXYtan = tracker_settings.get<float>("tracking settings.CxyTan");
		ball_tracker.parameters.calibrCZ = tracker_settings.get<float>("tracking settings.Cz");

		float valXYZ2ArenaMat[9] = {0, 0, -3.0, 0, 0, 0, 3.0, 0, 0,};
		float valXYZ2Yaw[3]      = {0, -57.3248, 0};


		ballXYZtoArenaXYZMat = cv::Mat(3, 3, CV_32F, valXYZ2ArenaMat);
		ballXYZtoArenaYaw    = cv::Mat(3, 1, CV_32F, valXYZ2Yaw);
		gainWalk             = 200;//50
		gainTurn             = 10;

		/********** DEBUG TRANSFORMATIONS MATRICES ************/
		std::cout << "Matrix ball to arena position tranform" << std::endl;
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				std::cout << ballXYZtoArenaXYZMat.at<float>(i,j) << " ";
			}
			std::cout << std::endl;
		}

		std::cout << "Vector ball to arena Yaw tranform" << std::endl;
		for(int i=0;i<3;i++){
			std::cout << ballXYZtoArenaYaw.at<float>(i) << " ";
		}
		/******************************************************/

		return true;
	}
	catch (std::exception& e)
	{
		std::cerr << boost::current_exception_diagnostic_information() << std::endl;
		std::cout << "Error reading tracking configuration file" << std::endl;
		std::cout << e.what() << std::endl;
		return false;
	}
}



/************* callbacks *******************/

/*
void BallTracking::subCallback(const std_msgs::String::ConstPtr &msg){
	this->debug("SUBCALLBACK", msg->data);
}

*/



/************ main loop ****************/
void BallTrackingNode::mainNodeThread(){
	c++;
	imageAcquisition.grabOne(frame, frame_timestamp);  // waits for newest frame and returns it
													   // comment out to get full frame period, uncommented for tracking time only:
													   // prev_iter_time = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch());


    ball_stamp_msg.data = ros::Time::now().toNSec();
    this->ballStampPub.publish(ball_stamp_msg);

	track_data = ball_tracker.update(frame, fit_quality);
	//cv::imshow(camera_name, frame);

	std::chrono::microseconds current_time = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch());
	std::chrono::microseconds dt = current_time - prev_iter_time;
	prev_iter_time = current_time;
	runtimes.push_back(dt.count());
	if (c % 10 == 0) {
		cv::imshow("Tracking camera", frame);
		//cv::imshow("Tracking timings", timings_plot);

		int average = std::accumulate(runtimes.begin(), runtimes.end(), 0.0) / runtimes.size();
		runtimes.clear();
		//std::cout << "\r" << average << "               " << std::flush;


	}



	//trackdata is [amplitude, offset_tan, phase]

	trackingData.header.stamp = ros::Time::now();
	trackingData.point.x = std::cos(track_data.at<double>(2))*track_data.at<double>(0) / ball_tracker.parameters.calibrCXYtan;
	trackingData.point.y = -std::sin(track_data.at<double>(2))*track_data.at<double>(0) / ball_tracker.parameters.calibrCXYtan;
	trackingData.point.z = track_data.at<double>(1) / ball_tracker.parameters.calibrCZ;
	this->TrackingDataPub.publish(trackingData);


	//cv::Mat XYZdisplacement = gainWalk * ballXYZtoArenaXYZMat * ballRot;

	double XYZdisplacement[3] = {-3*gainWalk*trackingData.point.z, 0, 3*gainWalk*trackingData.point.x};
	double YawDisplacement = gainTurn * -57.3248 * trackingData.point.y;         //ballXYZtoArenaYaw.dot(ballRot);

	//velCmd.header.stamp = ros::Time::now();
	//velCmd.twist.linear.x = XYZdisplacement.at<float>(0);
	//velCmd.twist.linear.y = XYZdisplacement.at<float>(1);
	//velCmd.twist.linear.z = XYZdisplacement.at<float>(2);
	//velCmd.twist.angular.z = YawDisplacement;
	velCmd.linear.x = -XYZdisplacement[2];//XYZdisplacement.at<float>(2);
	velCmd.linear.y = XYZdisplacement[0];//XYZdisplacement.at<float>(1);
	velCmd.linear.z = XYZdisplacement[1];//XYZdisplacement.at<float>(1);
	velCmd.angular.z = YawDisplacement;

	velCmdMsg.twist = velCmd;
	velCmdMsg.header.stamp = ros::Time::now();

	this->velCmdPub.publish(velCmdMsg);

    double disp_dt = (ros::Time::now() - this->timer_disp).toSec();
    this->timer_disp = ros::Time::now();

    dispCmd.linear.x = dispCmd.linear.x + velCmd.linear.x * disp_dt;
    dispCmd.linear.y = dispCmd.linear.y + velCmd.linear.y * disp_dt;
	dispCmd.angular.z = dispCmd.angular.z - velCmd.angular.z * disp_dt;
	this->DisplacementPub.publish(dispCmd);




	/*********** DEBUG **********/
	/*
	std::cout << YawDisplacement << std::endl;

	for(int i=0;i<3;i++){
		std::cout << XYZdisplacement.at<float>(i) << " ";
	}
	std::cout << std::endl;
	/****************************/



	char k = cv::waitKey(1);


}





void BallTrackingNode::debug(std::string type, std::string msg){
	std::cout << type << ": " << msg << std::endl;
}


int main(int argc, char** argv){
	ros::init(argc, argv,"ball_tracking_node");
	ros::NodeHandle* nh = new ros::NodeHandle();
	ros::Rate loop_rate(1000);


	BallTrackingNode node(nh);
	
	while (ros::ok()){
		node.mainNodeThread();
		//ros::spinOnce();
		//loop_rate.sleep();
	}
	return 0;
}
